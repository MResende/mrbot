# Robos.im Fullstack Challenge
The code in this repository was developed for the challenge in https://gist.github.com/rafaelamorim/0a6743ca98edc28bc8801fdb61d92a63.

The project was built using docker, you're going to need it to run. Find info on how to install it here: https://docs.docker.com/engine/installation/. Also, the Telegram API implementation was done using webhooks, use ngrok to get a domain for your machine, information on how to install it here: https://ngrok.com/download.

To run it, start ngrok use the command `ngrok http 3000`, then input the _HTTPS_ url in your terminal into `variables.env` inside the project. Then, simply run `docker-compose up --build`. Docker-compose 3.0 was used to wire up everything together (client/server/db).

To test, add to your Telegram account the bot named `@ResendeBot`. Then start it and you should already receive messages in your server running locally.

## Tech stack
  - Server:
    - MongoDB & Mongoose

      _Version 4.13.9_

      This was a personal choice, the database could have been an SQL one, but I choose MongoDB because it's easier to deal with documents since BSON and JSON are pretty similar.

    - NodeJS

      _Version 9.3_

    - Express

      _Version 4.16.2_

    - Socket.io

      _Version 2.04_

      The choice of the websocket used to keep a duplex connection alive between the client and server and, therefore, allow the server to post data for the client.

    - node-telegram-bot-api

      _Version 0.30.0_


  - Client:
    - ReactJS

      _Version 16.2.0_

      React was used because a 'realtime' chat consists in a lot of changes to the screen all the time, and one of the main reasons the framework was born is because of that. Also it encapsulates components wich make it easier to control.

    - Socket.io-client

      _Version 2.0.4_

      To match with the server and communicate with it.


    - Sass

      _Version 3.5.4_


OBS: The code was linted according to Airbnb's rules for client and server respectively.

