import React from 'react';

import './styles/Header.css';

const Header = function Header() {
  return (
    <header className="App-header">
      <h1 className="App-title">Bot Admin</h1>
    </header>
  );
};

export default Header;
