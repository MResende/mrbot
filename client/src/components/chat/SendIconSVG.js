import React from 'react';

const SendIcon = function SendIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      version="1.1"
      id="Layer_1"
      x="0px"
      y="0px"
      viewBox="0 0 512 512"
      style={{ enableBackground: 'new 0 0 512 512' }}
      xmlSpace="preserve"
      width="24px"
      height="24px"
      className=""
    >
      <g>
        <g>
          <g>
            <g>
              <path
                d={'M0,234.919l174.682,102.399l102.399,174.682L512,0.001L0,234.919z' +
                  'M275.387,478.16l-85.176-145.304l52.097-52.097' +
                  'l-11.068-11.068l-52.098,52.098L33.84,236.612L459.726,41.206L293.249,' +
                  '207.681l11.068,11.068L470.795,52.275L275.387,478.16z'}
                data-original="#000000"
                className="active-path"
                data-old_color="#2aadf7"
                fill="#2aadf7"
              />
              <rect
                x="257.132"
                y="223.121"
                transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 277.6292 609.0733)"
                width="15.652"
                height="47.834"
                data-original="#000000"
                className="active-path"
                data-old_color="#2aadf7"
                fill="#2aadf7"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default SendIcon;
