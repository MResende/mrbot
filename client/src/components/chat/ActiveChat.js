import React, { Component } from 'react';
import PropTypes from 'prop-types';

import sendIcon from './SendIconSVG';

class ActiveChat extends Component {
  static getBalloonOwner(message) {
    return typeof message.from !== 'undefined' ? 'from-contact' : 'from-myself';
  }

  static renderSingleMessage(message, i) {
    const className = ActiveChat.getBalloonOwner(message);
    const timestamp = (new Date(message.created_at)).toString().slice(0, 21).replace(/-/g, '');
    return (
      <div key={i} className={`ActiveChat__message-item--container ${className}`}>
        <div className="balloon">
          <span className="message-body">{message.text}</span><br />
          <span className="message-timestamp">{timestamp}</span><br />
        </div>
      </div>
    );
  }

  static noMessagesContent() {
    return (
      <h1>No Messages Yet</h1>
    );
  }

  constructor(props) {
    super(props);
    this.handleSendMessage = this.handleSendMessage.bind(this);
    this.handleEnterKey = this.handleEnterKey.bind(this);
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  handleSendMessage() {
    const sampleMessage = this.props.messages[0];
    const now = new Date().getTime();
    const newMessage = {
      to: { _id: sampleMessage.from._id },
      text: this.messageText.value,
      chat_id: sampleMessage.from.chat_id,
      created_at: now,
    };

    this.props.parentEmitMessage(newMessage);
    this.messageText.value = '';
  }

  handleEnterKey(e) {
    if (e.key === 'Enter') this.handleSendMessage();
  }

  scrollToBottom() {
    this.messagesEnd.scrollIntoView();
  }

  renderMessages() {
    let content;
    if (this.props.messages.length > 0) {
      content = this.props.messages.map((message, i) => ActiveChat.renderSingleMessage(message, i));
    } else {
      content = ActiveChat.noMessagesContent();
    }
    return content;
  }

  render() {
    return (
      <div className="Chat__active-chat--wrapper">
        <div className="ActiveChat__message-history--container">
          <div
            className="ActiveChat__message-history--wrapper"
            ref={(el) => { this.chatHistory = el; }}
          >
            {this.renderMessages()}
            <div
              style={{ float: 'left', clear: 'both' }}
              ref={(el) => { this.messagesEnd = el; }}
            />
          </div>
        </div>
        <div className="ActiveChat__new-message--container">
          <div className="ActiveChat__new-message--wrapper">
            <div className="ActiveChat__new-message--input-container">
              <input
                className="ActiveChat__new-message--input"
                ref={(el) => { this.messageText = el; }}
                onKeyPress={this.handleEnterKey}
              />
            </div>
            <div className="ActiveChat__new-message--send">
              <button
                onClick={this.handleSendMessage}
                type="sumbit"
                id="send-message"
              >
                {sendIcon()}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ActiveChat.propTypes = {
  parentEmitMessage: PropTypes.func.isRequired,
  messages: PropTypes.arrayOf(PropTypes.shape({
    from: PropTypes.shape({
      first_name: PropTypes.string.isRequired,
      last_name: PropTypes.string.isRequired,
      chat_id: PropTypes.number.isRequired,
      username: PropTypes.string,
    }),
    to: PropTypes.shape({
      first_name: PropTypes.string.isRequired,
      last_name: PropTypes.string.isRequired,
      chat_id: PropTypes.number.isRequired,
      username: PropTypes.string,
    }),
    text: PropTypes.string,
    created_at: PropTypes.string.isRequired,
  })).isRequired,
};

export default ActiveChat;
