import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ContactList extends Component {
  constructor(props) {
    super(props);

    this.handleSwitchFocus = this.handleSwitchFocus.bind(this);
  }

  handleSwitchFocus(event) {
    this.props.parentSwitchFocus(event.target.dataset.username);
  }

  renderContact(contact, i) {
    const className = contact.chat_id === this.props.currentChatId ? 'active' : '';
    return (
      <li key={i} className="Chat__contact-item">
        <button
          onClick={this.handleSwitchFocus}
          data-username={contact.username}
          data-chatid={contact.chatId}
          className={className}
        >
          <span className="Chat__contact-name">{contact.first_name} {contact.last_name}</span>
          <br />
          <span className="Chat__contact-username">{contact.username}</span>
        </button>
      </li>
    );
  }

  renderContacts() {
    return (
      <ul className="Chat__contact-list">
        {this.props.contacts.map((contact, i) => this.renderContact(contact, i))}
      </ul>
    );
  }

  render() {
    return (
      <div className="Chat__contact-list--wrapper">
        {this.renderContacts()}
      </div>
    );
  }
}

ContactList.propTypes = {
  currentChatId: PropTypes.number.isRequired,
  parentSwitchFocus: PropTypes.func.isRequired,
  contacts: PropTypes.arrayOf(PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    chat_id: PropTypes.number.isRequired,
    username: PropTypes.string,
    active: PropTypes.bool,
  })).isRequired,
};

export default ContactList;
