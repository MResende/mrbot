import React, { Component } from 'react';
import Axios from 'axios';
import socketIOClient from 'socket.io-client';

import ContactList from './ContactList';
import ActiveChat from './ActiveChat';
import './styles/Chat.css';

class Chat extends Component {
  constructor() {
    super();
    this.state = {
      currentChatId: 0,
      contacts: [],
      messages: [],
    };
    this.switchChatFocus = this.switchChatFocus.bind(this);
    this.emitNewMessage = this.emitNewMessage.bind(this);
  }

  componentDidMount() {
    this.getMessagesFromCurrentChat();
    this.socket = socketIOClient('http://localhost:3002');
    this.socket.on('messageReceived', (obj) => {
      const {
        chatId, message, contact, newContact,
      } = obj;

      const newState = this.state;

      if (newContact) {
        if (newState.contacts.length <= 0) {
          newState.currentChatId = contact.chat_id;
        }
        newState.contacts = newState.contacts.concat(contact);
      }

      if (chatId === this.state.currentChatId) {
        newState.messages = newState.messages.concat(message);
      }

      this.setState(newState);
    });
  }

  getMessagesFromCurrentChat() {
    Axios
      .get('/api/contacts')
      .then((contactsRes) => {
        const currentContact = contactsRes.data.contactList[0];
        const { contactList } = contactsRes.data;
        if (contactList.length > 0) return this.getMessages(currentContact, contactList);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getMessages(currentContact, contactList) {
    Axios
      .get(`/api/${currentContact._id}/messages`)
      .then((messagesRes) => {
        this.setState({
          currentChatId: currentContact.chat_id,
          contacts: contactList,
          messages: messagesRes.data.messages,
        });
      })
      .catch(err => new Error(err));
  }

  emitNewMessage(message) {
    this.socket.emit('messageSent', { message });
  }

  switchChatFocus(username) {
    const contact = this.state.contacts.filter(item => item.username === username)[0];
    if (contact.chat_id === this.state.currentChatId) return;
    this.getMessages(contact, this.state.contacts);
  }

  render() {
    return (
      <div className="Chat__container">
        <div className="Chat__contact-list--container">
          <ContactList
            currentChatId={this.state.currentChatId}
            parentSwitchFocus={this.switchChatFocus}
            contacts={this.state.contacts}
          />
        </div>
        <div className="Chat__active-chat--container">
          <ActiveChat
            parentEmitMessage={this.emitNewMessage}
            messages={this.state.messages}
          />
        </div>
      </div>
    );
  }
}

export default Chat;
