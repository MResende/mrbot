import React from 'react';
import AccountKit from 'react-facebook-account-kit';

const Login = function Login() {
  return (
    <div>
      <div>
        <h1>Bot admin</h1>
      </div>
      <div>
        <AccountKit
          appId="328535897552851" // Update this! 
          version="v1.0" // Version must be in form v{major}.{minor} 
          onResponse={resp => console.log(resp)}
          csrf={'csrf token here!'} // Required for security 
          loginType="email"
        >
          {p => <button {...p}>Login</button>}
        </AccountKit>
      </div>
    </div>
  );
};

export default Login;
