import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Login from '../login/Login';
import Chat from '../chat/Chat';
import Header from '../header/Header';

import './styles/App.css';


const App = function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route path="/login" component={Login} />
        <Route exact path="/" component={Chat} />
      </Switch>
    </div>
  );
};


export default App;
