
const bodyParser = require('body-parser');
const express = require('express');
const TelegramBot = require('node-telegram-bot-api');
const SocketIO = require('socket.io');
const http = require('http');

const config = require('./config/config');
const setupDB = require('./config/db');
const userController = require('./modules/users/controller');
const messageController = require('./modules/messages/controller');

/* eslint no-console: ["error", {allow: ["warn", "error"]}] */

const {
  port,
  dbUrl,
  telegramApiToken,
  url,
  socketPort,
} = config;
const app = express();
const server = http.Server(app);
const bot = new TelegramBot(telegramApiToken);
const io = SocketIO(server);


io.on('connection', (socket) => {
  socket.on('messageSent', (data) => {
    const user = data.message.to;
    const chatId = data.message.chat_id;

    messageController
      .sendMessage(data.message, user)
      .then((record) => {
        if (record) {
          io.emit('messageReceived', { chatId, message: record });
          bot.sendMessage(chatId, data.message.text);
        }
      })
      .catch(err => console.warn(err));
  });
});

// initialize bodyParser for json req/res
app.use(bodyParser.json());

// initialize database connection and the bot user
setupDB.init(dbUrl);

// setup bot webhook
bot.setWebHook(`${url}/bot${telegramApiToken}`);

// intialize bot route to receive messages
app.post(`/bot${telegramApiToken}`, (req, res) => {
  const { chat } = req.body.message;
  // retrieve or create user when receiving messages
  userController
    .processUser(chat)
    .then((obj) => {
      const { contact, newContact } = obj;
      // save message
      messageController
        .processMessage(req.body.message, contact)
        .then((message) => {
          const populatedMessage = message;
          populatedMessage.from = contact;
          if (message) {
            io.emit('messageReceived', {
              newContact,
              contact,
              chatId: contact.chat_id,
              message: populatedMessage,
            });
            res.sendStatus(200);
            return;
          }
          res.sendStatus(500);
        })
        .catch((err) => {
          console.warn(err);
          res.sendStatus(202);
        });
    })
    .catch((err) => {
      console.warn('err: ', err);
      res.status(500).send(err);
    });
});

// route to check connection
app.get('/api/ping', (req, res) => {
  res.status(200).send({ data: 'Pong!' });
});

app.get('/api/contacts', (req, res) => {
  userController
    .getMostRecentContacts()
    .then(contactList => res.status(200).send({ contactList }))
    .catch(err => res.status(500).send(err));
});

app.get('/api/:userId/messages', (req, res) => {
  const { userId } = req.params;
  messageController
    .getMessagesFrom(userId)
    .then(messages => res.send({ messages }))
    .catch(err => res.status(500).send(err));
});

io.listen(socketPort);
app.listen(port);

module.exports = { app };
