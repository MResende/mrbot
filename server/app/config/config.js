const getDbUrl = () => {
  const env = process.env.NODE_ENV || 'development';
  return env === 'test' ? process.env.MONGO_TEST_URL : process.env.MONGO_DEV_URL;
};

const config = {
  port: 3001,
  socketPort: 3002,
  dbUrl: getDbUrl(),
  telegramApiToken: process.env.TELEGRAM_API_TOKEN,
  url: process.env.NGROK_URL,
};

module.exports = config;
