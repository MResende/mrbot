const mongoose = require('mongoose');

const { User } = require('../modules/users/model');

const db = {};

const initBotUser = () => {
  User
    .findByUsername('mrbot')
    .then((data) => {
      if (!data) {
        const now = new Date();
        const mrBotUser = User({
          first_name: 'Mr',
          last_name: 'Bot',
          username: 'mrbot',
          created_at: now,
          updated_at: now,
        });
        User.addUser(mrBotUser);
      }
    })
    .catch(err => new Error(err));
};

db.init = (dbUrl) => {
  mongoose.Promise = global.Promise;
  mongoose.connect(dbUrl, { useMongoClient: true });
  initBotUser();
};


module.exports = db;
