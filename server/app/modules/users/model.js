const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name: {
    type: String,
    required: true,
  },
  username: {
    type: String,
  },
  chat_id: {
    type: Number,
    unique: true,
    index: true,
  },
  created_at: {
    type: Date,
    required: true,
  },
  updated_at: {
    type: Date,
    required: true,
  },
});

const User = mongoose.model('User', UserSchema);

User.findAll = () => User.find({}).where('username').ne('mrbot').sort({ updated_at: -1 });

User.touch = _id => User.find({ _id }).update({ updated_at: new Date() });

User.findByUsername = username => User.findOne({ username });

User.findByChatId = chatId => User.findOne({ chat_id: chatId });

User.addUser = newUser => newUser.save();

module.exports = { User };
