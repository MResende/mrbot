const { User } = require('./model');

const controller = {};

controller.processUser = chat => new Promise((resolve, reject) => {
  User
    .findByChatId(chat.id)
    .then((data) => {
      if (data && typeof data !== 'undefined') {
        User.touch(data._id)
          .then(() => {
            resolve({ contact: data, newContact: false });
          })
          .catch(err => reject(new Error(err)));
      } else {
        const now = new Date();
        const newUser = User({
          first_name: chat.first_name,
          last_name: chat.last_name,
          username: chat.username,
          chat_id: chat.id,
          created_at: now,
          updated_at: now,
        });

        User
          .addUser(newUser)
          .then(record => resolve({ contact: record, newContact: true }))
          .catch(err => reject(new Error(err)));
      }
    })
    .catch(err => reject(err));
});

controller.getMostRecentContacts = () => new Promise((resolve, reject) => {
  User
    .findAll()
    .then(data => resolve(data))
    .catch(err => reject(err));
});

module.exports = controller;
