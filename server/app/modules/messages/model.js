const mongoose = require('mongoose');

const MessageSchema = mongoose.Schema({
  from: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  created_at: {
    type: Date,
    required: true,
  },
  text: {
    type: String,
  },
  to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Message = mongoose.model('Message', MessageSchema);

Message.addMessage = newMessage => newMessage.save();

Message.findByUser = _id => Message
  .find({})
  .or([{ from: _id }, { to: _id }])
  .populate('from')
  .populate('to');

module.exports = { Message };
