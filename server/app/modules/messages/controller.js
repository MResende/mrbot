const { Message } = require('./model');

const controller = {};

controller.sendMessage = (message, user) => new Promise((resolve, reject) => {
  const newMessage = Message({
    to: user,
    created_at: message.created_at,
    text: message.text,
  });
  Message
    .addMessage(newMessage)
    .then((res) => {
      res
        .populate('to')
        .execPopulate()
        .then(doc => resolve(doc))
        .catch(err => reject(err));
    })
    .catch(err => reject(err));
});

controller.processMessage = (message, user) => new Promise((resolve, reject) => {
  if (typeof message.file === 'undefined'
      && typeof message.voice === 'undefined'
      && typeof message.text !== 'undefined') {
    const newMessage = Message({
      from: user._id,
      // js date uses miliseconds, telegram date is unix time
      created_at: new Date(message.date * 1000),
      text: message.text,
    });

    resolve(Message.addMessage(newMessage));
  }
  reject(new Error('Don\'t know what to do with voice/file messages'));
});

controller.getMessagesFrom = userId => Message.findByUser(userId);

module.exports = controller;
