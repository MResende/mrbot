const expect = require('expect');
const request = require('supertest');
const mongoose = require('mongoose');
const SocketIOClient = require('socket.io-client');

const { app } = require('../app/server');
const config = require('../app/config/config');
const mockData = require('./mockData.json');

beforeEach((done) => {
  if (typeof mongoose.connection.db !== 'undefined') {
    mongoose.connection.db.dropDatabase().then(() => done());
  }
});

afterAll((done) => {
  mongoose.connection.db.dropDatabase().then(() => done());
});

describe('--SERVER--', () => {
  describe('POST /botTELEGRAM_API_TOKEN', () => {
    it('should handle updates', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.text)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({});
        })
        .end(done);
    });

    it('should return 202 (Accepted) for files', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.file)
        .expect(202)
        .expect((res) => {
          expect(res.body).toEqual({});
        })
        .end(done);
    });

    it('should return 202 (Accepted) for stickers', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.voice)
        .expect(202)
        .expect((res) => {
          expect(res.body).toEqual({});
        })
        .end(done);
    });

    it('should return 202 (Accepted) for voice', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.sticker)
        .expect(202)
        .expect((res) => {
          expect(res.body).toEqual({});
        })
        .end(done);
    });
  });

  describe('GET /api/contacts', () => {
    it('should retrieve the list of users in the database', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.text)
        .then(() => {
          request(app)
            .get('/api/contacts')
            .expect(200)
            .expect((res) => {
              expect(res.body.contactList.length).toBe(1);
              const contact = res.body.contactList[0];
              expect(contact.first_name).toBe('Darth');
              expect(contact.last_name).toBe('Vader');
              expect(contact.chat_id).toBe(426775639);
              expect(contact.username).toBe('DarthVader');
            })
            .end(done);
        });
    });
  });

  describe('GET /api/:userId/messages', () => {
    it('should return the messages of the user', (done) => {
      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.text)
        .then(() => {
          request(app)
            .get('/api/contacts')
            .then((res) => {
              const userId = res.body.contactList[0]._id;
              request(app)
                .get(`/api/${userId}/messages`)
                .expect(200)
                .expect((messagesRes) => {
                  expect(messagesRes.body.messages.length).toBe(1);
                  const message = messagesRes.body.messages[0];
                  expect(message.from._id).toBe(userId);
                  expect(message.text).toBe(mockData.text.message.text);
                  const date = new Date(message.created_at).getTime() / 1000;
                  expect(date).toBe(mockData.text.message.date);
                })
                .end(done);
            });
        });
    });
  });
});

describe('--SOCKET IO--', () => {
  const socket = SocketIOClient('http://localhost:3002');

  describe('#messageReceived', () => {
    it('should emit messages after calling GET /botTOKEN', (done) => {
      socket.on('messageReceived', (obj) => {
        const {
          chatId, message, contact, newContact,
        } = obj;
        expect(chatId).toBe(mockData.text.message.chat.id);
        expect(!!newContact).toBeTruthy();
        expect(message.text).toBe('I\'m your father');
        expect(message.from.username).toBe('DarthVader');
        expect(contact.username).toBe('DarthVader');
      });

      request(app)
        .post(`/bot${config.telegramApiToken}`)
        .send(mockData.text)
        .end(done);
    });
  });
});
